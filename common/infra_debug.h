/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef INFRA_DEBUG_H_
#define INFRA_DEBUG_H_

#define INFRA_ERROR_TRACE_COMPILE	1
#define INFRA_ERROR_TRACE_RUNTIME	0
#define INFRA_DEBUG_TRACE_COMPILE	0
#define INFRA_DEBUG_TRACE_RUNTIME	0
#define INFRA_TEST_ERROR_TRACE		1
#define INFRA_TEST_DEBUG_TRACE		1

typedef enum {
	INFRA_WRAPPER_DEBUG_ID = 0,
	INFRA_EVENT_DEBUG_ID ,
	INFRA_REGISTRY_DEBUG_ID,
	INFRA_MSS_DEBUG_ID,
	INFRA_SCR_DEBUG_ID,
	INFRA_CEC_DEBUG_ID,
	INFRA_DEBUG_ID_MAX
}infra_debug_id_t;

static inline __attribute__ ((format (printf, 1, 2)))
int no_print(const char *s, ...)
{
        return 0;
}

#define infra_trace_compile(enable, fmt, ...)	do { \
							if(enable){ \
								printk("<%s:%d>:: ",__FUNCTION__, __LINE__); \
								printk(fmt,  ##__VA_ARGS__); \
							} \
						} while(0)

#define infra_trace_runtime(id, enable, fmt, ...)	do { \
							if(infra_arr_debug[id][##enable##_RUNTIME]) {\
								printk("<%s:%d>:: ",__FUNCTION__, __LINE__); \
								printk(fmt,  ##__VA_ARGS__); \
							} \
						} while(0)


#if INFRA_ERROR_TRACE_COMPILE
#define infra_error_trace(id, enable, fmt, ...)	infra_trace_compile(enable, fmt, ...)
#elif INFRA_ERROR_TRACE_RUNTIME
#define infra_error_trace(id, enable, fmt, ...)	infra_trace_runtime(id, enable, fmt, ...)
#else
#define infra_error_trace(id, enable, fmt, ...) 	while(0)
#endif



#if INFRA_DEBUG_TRACE_COMPILE
#define infra_debug_trace(id, enable, fmt, ...)		infra_trace_compile(enable, fmt, ...)
#elif INFRA_ERROR_TRACE_RUNTIME
#define infra_debug_trace(id, enable, fmt, ...)		infra_trace_runtime(id, enable, fmt, ...)
#else
#define infra_debug_trace(id, enable, fmt, ...) 	while(0)
#endif

#if INFRA_TEST_ERROR_TRACE
#define infra_test_error_trace(id, enable, fmt, ...)	infra_trace_compile(enable, fmt, ...)
#else
#define infra_test_error_trace(id, enable, fmt, ...) 	while(0)
#endif

#if INFRA_TEST_DEBUG_TRACE
#define infra_test_debug_trace(id, enable, fmt, ...)	infra_trace_compile(enable, fmt, ...)
#else
#define infra_test_debug_trace(id, enable, fmt, ...) 	while(0)
#endif


#endif /*INFRA_DEBUG_H_*/
