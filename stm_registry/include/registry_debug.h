/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef REGISTRY_DEBUG_H_
#define REGISTRY_DEBUG_H_

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/list.h>
#include "infra_os_wrapper.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Enable/disable debug-level macros.
 *
 * This really should be off by default if you
 * *really* need your message to hit the console every time then that is what REGISTRY_TRACE()
 * is for.
 */

/* Comment in release version */
/*#define DEBUG_VER	1 */


#ifdef DEBUG_VER
#define ENABLE_REGISTRY_DEBUG      1
#else
#define ENABLE_REGISTRY_DEBUG      0
#endif

#define REG_STKPI		1
#define REG_RBTREE		1
#define REG_SYSFS		1
#define REG_DEBUGFS		1
#define REG_CORE		1


#define REGISTRY_DEBUG_MSG(fmt, args...)	 ((void) (ENABLE_REGISTRY_DEBUG && \
						 (pr_info("%s: " fmt, __FUNCTION__, ##args), 0)))

/* Output trace information off the critical path */
#define REGISTRY_TRACE_MSG(fmt, args...)         (pr_notice("%s: " fmt, __FUNCTION__, ##args))

/* Output errors, should never be output in 'normal' operation */
#define REGISTRY_ERROR_MSG(fmt, args...)         pr_crit("ERROR in %s %d: " fmt, __FUNCTION__, __LINE__, ##args)

#define REGISTRY_ASSERT(x) do { \
					if (!(x)) \
						pr_crit("%s: Assertion '%s' failed at %s:%d\n", \
						__FUNCTION__, #x, __FILE__, __LINE__); \
				} while (0)


#define RED   "\033[0;31m"
#define CYAN  "\033[1;36m"
#define GREEN "\033[4;32m"
#define BLUE  "\033[9;34m"
#define NONE   "\033[0m"
#define BROWN  "\033[0;33m"
#define MAGENTA  "\033[0;35m"

#ifdef __cplusplus
}
#endif

#endif /* __REGISTRY_DEBUG_H */


