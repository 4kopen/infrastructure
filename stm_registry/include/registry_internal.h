/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef _REGISTRY_INTERNAL_H
#define _REGISTRY_INTERNAL_H

#include <linux/string.h>
#include <linux/kobject.h>

#include "infra_os_wrapper.h"
#include "stm_registry.h"
#include <linux/rbtree.h>
#include "infra_debug.h"
#include "registry_debug.h"


#ifdef __cplusplus
extern "C" {
#endif


#define STM_REGISTRY_ROOT_KOBJECT     "stkpi"
#define REG_CONNECTION_SYMLINK_TAG_MAX_LEN     100
/* REG_SYSLINK_MAX_TAG_SIZE is set to size of (STM_REGISTRY_MAX_TAG_SIZE + 5)
 * to use 32 characters of the tag and is prefixed with "inst_" to add to sysfs.
*/
#define REG_SYSLINK_MAX_TAG_SIZE 45


/* for children list */
struct registrynode_ctx {
	struct registrynode *registry_p;
	struct registrynode_ctx			*node_next_np;
};

struct connection_info_s {
	struct registrynode *connected_node;
	char tag_p[STM_REGISTRY_MAX_TAG_SIZE];
};

struct connection_node_s {
	struct connection_info_s connect_info;
	struct connection_node_s	    *connection_next_np;
};

struct attribute_info_s {
	void *buffer_p;
	int usize;
	char tag_p[STM_REGISTRY_MAX_TAG_SIZE];
	char datatypetag_p[STM_REGISTRY_MAX_TAG_SIZE];
};

struct attribute_node_s {
	struct attribute_info_s attribute_info;
#if (defined SDK2_REGISTRY_ENABLE_SYSFS_ATTRIBUTES)
	struct kobj_attribute	 reg_attr;
#endif
	struct registrynode *Registrynode;
	struct attribute_node_s *attribute_next_np;
};


struct object_info_s {
	const char *tag_p;
	stm_object_h *object_p;
};

struct objecttype_info_s {
	stm_object_h object_type_h;
	char objecttypetag_p[STM_REGISTRY_MAX_TAG_SIZE];
};

/*
  * building block of registry hierarchy.
  * Each and every registry node is represented by single registrynode.
  * As long as s_count reference is held, the regsitrynode itseld is
  * accessible.
  */
struct registrynode {
	struct registrynode *Parent_p;
	struct rb_node r_rbnode;
	stm_object_h parent_h;
	stm_object_h object_h;
	char *tag_p;

	struct registrynode_ctx *children_np;
	struct connection_node_s *connection_np;
	struct attribute_node_s *attribute_np;
	struct objecttype_info_s object_type;

	struct kref refcount;
	void (*release)(struct kref *kref);
	infra_os_semaphore_t sem_lock;
	infra_os_semaphore_t no_active_usr_sem_p;
	uint32_t connected_obj_count;
	bool deletion_started;
#if (defined SDK2_REGISTRY_ENABLE_SYSFS_ATTRIBUTES)
	struct kobject *kobj;
#endif
};

typedef enum {
	/* Flag bit relation*/
	REGISTRY_TAG = 1, /* 0000001 */
	REGISTRY_PARENT = 2, /* 0000010 */
	REGISTRY_OBJECT = 4, /* 0000100 */
	REGISTRY_CONNECTION = 8, /* 0001000 */
	REGISTRY_ATTRIBUTE = 16, /* 0010000 */
	REGISTRY_OBJECT_TYPE = 32, /* 0100000 */
}
Info_type_flag;

extern struct registrynode registry_root;


int take_regnode_lock(struct registrynode *regnode);
int release_regnode_lock(struct registrynode *regnode);

/*!
 *\brief increment use count on object
 */
static inline void __reg_count_inc(struct registrynode *regnode)
{
	kref_get(&regnode->refcount);

	pr_debug("Inc Registry Node 0x%p ref count %d\n",
			regnode, atomic_read(&regnode->refcount.refcount));
}

/*!
 *\brief decrement use count on object, frees object if now unused.
 */
static inline int __reg_count_dec(struct registrynode *regnode)
{
	if (!regnode)
		return 1;

	pr_debug("Dec Registry Node 0x%p ref count %d",
			regnode, atomic_read(&regnode->refcount.refcount));

	return kref_put(&regnode->refcount, regnode->release);
}

#define reg_count_inc(x)   __reg_count_inc(x)
#define reg_count_dec(x)   __reg_count_dec(x)

/*Registry internal API*/

int registry_internal_find_node(stm_object_h object, struct registrynode **Registrynode);

int registry_internal_alloc_node(struct registrynode **Registrynode);

int registry_internal_add_object(struct registrynode *Registrynode,
				 stm_object_h object,
				 stm_object_h parent);

int registry_internal_dealloc_node(struct registrynode *Registrynode);

int registry_internal_remove_object(struct registrynode *Registrynode, stm_object_h object);

int registry_internal_add_node_info(struct registrynode *Registrynode, Info_type_flag InfoFlag, void *Info_p);

int registry_internal_get_node_info(struct registrynode *Registrynode, Info_type_flag InfoFlag, void *Info_p);

int registry_internal_remove_node_info(struct registrynode *Registrynode, Info_type_flag InfoFlag, void *Info_p);

int registry_internal_clean_registry(void);



int registry_internal_fill_attribute_node(struct attribute_node_s **Attr_np,
					  struct attribute_info_s *Attrinfo_p,
					  struct registrynode *Registrynode);

int registry_internal_add_root(const char *tag,
			       stm_object_h object);
void registry_internal_delete_link(struct registrynode *Registrynode);

int registry_internal_create_link(struct registrynode *Registrynode_objtype,
				  struct registrynode *Registrynode);

void registry_internal_dump_registry(void);


/* to be used for deleting and adding the node from the registry link list (HeadRegistryNode_np)*/
#if (defined SDK2_REGISTRY_ENABLE_SYSFS_ATTRIBUTES)
int registry_internal_create_connection_symblink(struct registrynode *Registrynode,
						 struct connection_info_s *connectioninfo_p);
int registry_internal_del_connection_symblink(struct registrynode *Registrynode,
					      struct connection_info_s *connectioninfo_p);
#endif


int registry_get_ref(stm_object_h object, struct registrynode **Registry_p);

int registry_put_ref(struct registrynode *Registrynode);

int registry_internal_update_attribute(struct registrynode *Registrynode,
				    struct attribute_info_s *attribute_info);
int registry_internal_remove_connection(struct registrynode *Registrynode,
					const char *tag, struct registrynode **Registrynode_connected);

void registry_internal_connection_count_inc(struct registrynode  *Registry_p);
void registry_internal_connection_count_dec(struct registrynode  *Registry_p);
#ifdef __cplusplus
}
#endif

#endif
