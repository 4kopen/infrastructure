/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef _REGISTRY_ITERATOR_H
#define _REGISTRY_ITERATOR_H

#include "registry_internal.h"
#include "stm_registry.h"
#ifdef __cplusplus
extern "C" {
#endif
typedef struct stm_registry_iterator_s {
	stm_object_h			object_h;
	struct registrynode_ctx *children_np;
	struct connection_node_s *connection_np;
	struct attribute_node_s *attribute_np;
	char tag[STM_REGISTRY_MAX_TAG_SIZE];
	struct registrynode					*registry_node_p;
} stm_registry_iterator_t;



int registry_internal_new_iterator(struct registrynode *Registrynode,
				   stm_registry_member_type_t types,
				   stm_registry_iterator_h *p_iter);
int registry_internal_delete_iterator(stm_registry_iterator_h iter);

int registry_internal_get_next_iterator(stm_registry_iterator_h iter,
					char *tag,
					stm_registry_member_type_t *p_child_type);

#ifdef __cplusplus
}
#endif

#endif
