/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __REG_TREE_H
#define __REG_TREE_H

#include <linux/rbtree.h>
#include <registry_internal.h>

int	 registry_rbtree_insert(struct rb_root *root,
				struct registrynode *reg_node);

struct registrynode *registry_rbtree_search(struct rb_root *root,
					    stm_object_h Object);

int registry_rbtree_remove_node(struct rb_root *root,
				stm_object_h Object);

#endif /* __REG_TREE_H */

