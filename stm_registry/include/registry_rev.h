/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/
#ifndef __REGSITRY_REV_H
#define __REGISTRY_REV_H

#ifdef __cplusplus
extern "C" {
#endif

const char *REGISTRY_Revision = "STMREGISTRY-REL_1.0.0-SNAP_2011.05.121";

#ifdef __cplusplus
}
#endif

#endif /* #ifndef __REGISTRY_REV_H */
