/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef _REGISTRY_SYSFS_H
#define _REGISTRY_SYSFS_H

#if (defined SDK2_REGISTRY_ENABLE_SYSFS_ATTRIBUTES)
#include <linux/kobject.h>

#ifdef __cplusplus
extern "C" {
#endif


int registry_sysfs_add_attribute(struct attribute_node_s *attr_np,
				 struct kobject *kobj,
				 char *name);

int registry_sysfs_add_symblink(struct kobject *kobj,
				struct kobject *target,
				char *name);

int registry_sysfs_add_kobject(char *kobject_name,
			       struct kobject *parent_kobject,
			       struct kobject **registry_kobject);

void registry_sysfs_remove_attribute(struct kobject *kobj,
				     char *name);

void registry_sysfs_remove_symblink(struct kobject *kobj,
				    char *name);

void registry_sysfs_remove_kobject(struct kobject **reg_kobject);

int registry_sysfs_move_kobject(struct kobject *kobj,
				struct kobject *new_parent);

#ifdef __cplusplus
}
#endif

#endif
#endif
