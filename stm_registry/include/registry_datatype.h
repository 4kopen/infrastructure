/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef _REGISTRY_DATATYPE_H
#define _REGISTRY_DATATYPE_H

#include "stm_registry.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct registry_datatype_info_s {
	char tag[STM_REGISTRY_MAX_TAG_SIZE];
	stm_registry_type_def_t def;
} registry_datatype_info_t;

typedef struct registry_datatype_s {
	registry_datatype_info_t datatype_info;
	struct registry_datatype_s *datatype_next_np;
} registry_datatype_t;


int registry_internal_add_default_data_type(void);

int registry_internal_remove_default_data_type(void);

int registry_internal_add_data_type(const char *tag,
				    stm_registry_type_def_t *def);

int registry_internal_remove_data_type(const char *tag);

int registry_internal_get_data_type(const char *tag,
				    stm_registry_type_def_t *def);
#ifdef __cplusplus
}
#endif

#endif
