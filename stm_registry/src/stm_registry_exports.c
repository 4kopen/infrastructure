/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/*
 *    @file     stm_registry_exports.c
 *       @brief    Implementation for exporting Registry Export's functions
 *
 *        */


/* Requires   MODULE         defined on the command line */
/* Requires __KERNEL__       defined on the command line */
/* Requires __SMP__          defined on the command line for SMP systems   */
/* Requires EXPORT_SYMTAB    defined on the command line to export symbols */

#include <linux/init.h>    /* Initiliasation support */
#include <linux/module.h>  /* Module support */
#include <linux/kernel.h>  /* Kernel support */
#include <linux/version.h> /* Kernel version */

#include "stm_registry.h"


#define EXPORT_SYMTAB

EXPORT_SYMBOL(stm_registry_add_object);
EXPORT_SYMBOL(stm_registry_add_instance);
EXPORT_SYMBOL(stm_registry_remove_object);
EXPORT_SYMBOL(stm_registry_get_object_tag);
EXPORT_SYMBOL(stm_registry_get_object);
EXPORT_SYMBOL(stm_registry_get_object_parent);
EXPORT_SYMBOL(stm_registry_get_object_type);
EXPORT_SYMBOL(stm_registry_add_attribute);
EXPORT_SYMBOL(stm_registry_update_attribute);
EXPORT_SYMBOL(stm_registry_get_attribute);
EXPORT_SYMBOL(stm_registry_remove_attribute);
EXPORT_SYMBOL(stm_registry_add_connection);
EXPORT_SYMBOL(stm_registry_get_connection);
EXPORT_SYMBOL(stm_registry_remove_connection);
EXPORT_SYMBOL(stm_registry_add_data_type);
EXPORT_SYMBOL(stm_registry_get_data_type);
EXPORT_SYMBOL(stm_registry_remove_data_type);
EXPORT_SYMBOL(stm_registry_new_iterator);
EXPORT_SYMBOL(stm_registry_delete_iterator);
EXPORT_SYMBOL(stm_registry_iterator_get_next);
EXPORT_SYMBOL(stm_registry_dumptheregistry);
EXPORT_SYMBOL(stm_registry_types);
EXPORT_SYMBOL(stm_registry_instances);

