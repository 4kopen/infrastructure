/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include "registry_rbtree.h"
#include "registry_debug.h"

int registry_rbtree_insert(struct rb_root *root, struct registrynode *reg_node)

{
	struct rb_node **new_node = &root->rb_node;
	struct rb_node *parent_node = NULL;
	struct registrynode *this = NULL;

	REGISTRY_DEBUG_MSG("root (0x%p) , registrynode (0x%p)\n", root, reg_node);
	while (*new_node) {
		parent_node = *new_node;
		/*#define rb_entry(ptr, type, member) container_of(ptr, type, member)*/
		this = rb_entry(*new_node, struct registrynode, r_rbnode);

		if (reg_node->object_h < this->object_h)
			new_node = &(*new_node)->rb_left;
		else if (reg_node->object_h >= this->object_h)
			new_node = &(*new_node)->rb_right;
		else
			return false;
	}

	rb_link_node(&reg_node->r_rbnode, parent_node, new_node);
	rb_insert_color(&reg_node->r_rbnode, root);
	return true;
}



struct registrynode *registry_rbtree_search(struct rb_root *root, stm_object_h Object)
{


	struct rb_node *node = root->rb_node;
	struct registrynode *registry_entry;

	REGISTRY_DEBUG_MSG("root : 0x%p , Object : 0x%p", root, Object);
	while (node) {
		registry_entry = rb_entry(node, struct registrynode, r_rbnode);

		if (Object < registry_entry->object_h)
			node = node->rb_left;
		else if (Object > registry_entry->object_h)
			node = node->rb_right;
		else
			return registry_entry;
	}

	/* object not found in rbtree ; return NULL */
	return NULL;
}


int registry_rbtree_remove_node(struct rb_root *root, stm_object_h Object)
{
	struct registrynode *registry_entry;
	int err = 0;

	REGISTRY_DEBUG_MSG("Object is 0x%p\n", Object);

	registry_entry = registry_rbtree_search(root, Object);

	if (registry_entry) {

		REGISTRY_DEBUG_MSG("Erasing rb node of registry 0x%p , Object 0x%p\n", registry_entry, Object);
		rb_erase(&registry_entry->r_rbnode, root);
	} else {
		err = -ENODEV;
		return err;
	}

	return 0;
}



