/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <linux/string.h>

#include "registry_datatype.h"
#include "infra_os_wrapper.h"
#include "registry_internal.h"
extern registry_datatype_t *HeadDataType_np;
extern registry_datatype_t *HeadDataType_removable_np;

extern infra_os_semaphore_t g_registry_data_sem_lock;

/*Attribute will contain the function pointer only*/
static int registry_print_int32(stm_object_h object,
				char *buf,
				size_t size,
				char *user_buf,
				size_t user_size)
{
	int total_write = 0;
	const int *values = (const int *)buf;
	size_t count;

	if (!user_size || !user_buf)
		return 0;

	*user_buf = '\0';

	for(count = 0;
	    count < (size / sizeof(int)) && user_size;
	    count++) {
		int l = snprintf(user_buf, user_size, "%d\n", *values++);
		if ((size_t)l >= user_size) {
			total_write += user_size - 1;
			break;
		}

		user_buf += l;
		user_size -= l;
		total_write += l;
	}

	return total_write;
}

static int registry_print_uint32(stm_object_h object,
				 char *buf,
				 size_t size,
				 char *user_buf,
				 size_t user_size)
{
	int total_write = 0;
	const unsigned int *values = (const unsigned int *)buf;
	size_t count;

	if (!user_size || !user_buf)
		return 0;

	*user_buf = '\0';

	for(count = 0;
	    count < (size / sizeof(unsigned int)) && user_size;
	    count++) {
		int l = snprintf(user_buf, user_size, "%u\n", *values++);
		if ((size_t)l >= user_size) {
			total_write += user_size - 1;
			break;
		}

		user_buf += l;
		user_size -= l;
		total_write += l;
	}

	return total_write;
}

static int registry_print_address(stm_object_h object,
				  char *buf,
				  size_t size,
				  char *user_buf,
				  size_t user_size)
{
	int total_write = 0;
	const void **values = (const void **)buf;
	size_t count;

	if (!user_size || !user_buf)
		return 0;

	*user_buf = '\0';

	for(count = 0;
	    count < (size / sizeof(void *)) && user_size;
	    count++) {
		int l = snprintf(user_buf, user_size, "%pS\n", *values++);
		if ((size_t)l >= user_size) {
			total_write += user_size - 1;
			break;
		}

		user_buf += l;
		user_size -= l;
		total_write += l;
	}

	return total_write;
}



/*Attribute will contain the function pointer only*/
static int registry_store_int32(stm_object_h object,
				char *buf,
				size_t size,
				const char *user_buf,
				size_t user_size)
{

	/*"%d"*/

	REGISTRY_DEBUG_MSG("\n");

	sscanf(user_buf, "%d", (int32_t *) buf);

	return user_size;
}

static int registry_store_uint32(stm_object_h object,
				 char *buf,
				 size_t size,
				 const char *user_buf,
				 size_t user_size)
{
	/*"%u"*/

	REGISTRY_DEBUG_MSG("\n");
	sscanf(user_buf, "%u", (int32_t *) buf);

	return user_size;
}


static int registry_store_address(stm_object_h object,
				  char *buf,
				  size_t size,
				  const char *user_buf,
				  size_t user_size)
{
/*TODO : add store address support*/
	return 0;
}

int registry_internal_add_data_type(const char *Tag, stm_registry_type_def_t *Def)
{
	registry_datatype_t *datatype_trvs_np;
	registry_datatype_t *datatype_prev_np = NULL;
	registry_datatype_t *datatype_np = NULL;

	infra_os_sema_wait(&g_registry_data_sem_lock);
	datatype_trvs_np = HeadDataType_np;
	while (datatype_trvs_np) {
		if (!strcmp(datatype_trvs_np->datatype_info.tag, Tag)) {
			REGISTRY_ERROR_MSG("<%s>:<%d> Data type exist\n", __FUNCTION__, __LINE__);
			infra_os_sema_signal(&g_registry_data_sem_lock);
			return -EEXIST;
		}
		datatype_prev_np = datatype_trvs_np;
		datatype_trvs_np = datatype_trvs_np->datatype_next_np;
	}
	datatype_np = (registry_datatype_t *) infra_os_malloc(sizeof(registry_datatype_t));
	if (!datatype_np) {
		REGISTRY_ERROR_MSG("<%s>:<%d> Memory allocation failed\n", __FUNCTION__, __LINE__);
		infra_os_sema_signal(&g_registry_data_sem_lock);
		return -ENOMEM;
	}
	datatype_np->datatype_info.def.print_handler = Def->print_handler;
	datatype_np->datatype_info.def.store_handler = Def->store_handler;
	strlcpy(datatype_np->datatype_info.tag, Tag, STM_REGISTRY_MAX_TAG_SIZE);
	datatype_np->datatype_next_np = NULL;

	datatype_prev_np->datatype_next_np = datatype_np;

	if (datatype_trvs_np == HeadDataType_removable_np)
		HeadDataType_removable_np = datatype_np;

	infra_os_sema_signal(&g_registry_data_sem_lock);
	return 0;
}


int registry_internal_remove_data_type(const char *Tag)
{
	registry_datatype_t *datatype_np;
	registry_datatype_t *datatype_prev_np = NULL;
	/* Protection for default data type */

	infra_os_sema_wait(&g_registry_data_sem_lock);
	datatype_np = HeadDataType_np->datatype_next_np->datatype_next_np->datatype_next_np;
	datatype_prev_np = HeadDataType_np->datatype_next_np->datatype_next_np;
	while (datatype_np) {
		if (!strcmp(datatype_np->datatype_info.tag, Tag)) {
			datatype_prev_np->datatype_next_np = datatype_np->datatype_next_np;
			infra_os_free(datatype_np);
			break;
		}
		datatype_prev_np = datatype_np;
		datatype_np = datatype_np->datatype_next_np;
	}

	if (!datatype_np) {
		REGISTRY_ERROR_MSG("<%s>:<%d> Data type Does not exist\n", __FUNCTION__, __LINE__);
		infra_os_sema_signal(&g_registry_data_sem_lock);
		return -ENODEV;
	}
	infra_os_sema_signal(&g_registry_data_sem_lock);
	return 0;
}

int registry_internal_get_data_type(const char *Tag, stm_registry_type_def_t *Def)
{
	registry_datatype_t *datatype_np;

	infra_os_sema_wait(&g_registry_data_sem_lock);
	datatype_np = HeadDataType_np;

	while (datatype_np) {
		if (!strcmp(datatype_np->datatype_info.tag, Tag)) {
			Def->print_handler = datatype_np->datatype_info.def.print_handler;
			Def->store_handler = datatype_np->datatype_info.def.store_handler;
			break;
		}
		datatype_np = datatype_np->datatype_next_np;
	}
	if (!datatype_np) {
		REGISTRY_ERROR_MSG("<%s>:<%d> Data type Does not exist\n", __FUNCTION__, __LINE__);
		infra_os_sema_signal(&g_registry_data_sem_lock);
		return -ENODEV;
	}
	infra_os_sema_signal(&g_registry_data_sem_lock);
	return 0;
}


int registry_internal_add_default_data_type(void)
{
	registry_datatype_t *datatype_np;
	registry_datatype_t *datatype_prev_np;

	infra_os_sema_wait(&g_registry_data_sem_lock);
	datatype_np = HeadDataType_np;
	datatype_np->datatype_info.def.print_handler = registry_print_int32;
	datatype_np->datatype_info.def.store_handler = registry_store_int32;
	strlcpy(datatype_np->datatype_info.tag, STM_REGISTRY_INT32, (STM_REGISTRY_MAX_TAG_SIZE - 1));
	datatype_prev_np = datatype_np;

	datatype_np = (registry_datatype_t *) infra_os_malloc(sizeof(registry_datatype_t));
	if (!datatype_np) {
		infra_os_sema_signal(&g_registry_data_sem_lock);
		return -ENOMEM;
	}

	datatype_np->datatype_next_np = NULL;
	datatype_prev_np->datatype_next_np = datatype_np;
	datatype_np->datatype_info.def.print_handler = registry_print_uint32;
	datatype_np->datatype_info.def.store_handler = registry_store_uint32;
	strlcpy(datatype_np->datatype_info.tag, STM_REGISTRY_UINT32, (STM_REGISTRY_MAX_TAG_SIZE - 1));
	datatype_prev_np = datatype_np;

	datatype_np = (registry_datatype_t *) infra_os_malloc(sizeof(registry_datatype_t));
	if (!datatype_np) {
		infra_os_sema_signal(&g_registry_data_sem_lock);
		return -ENOMEM;
	}

	datatype_np->datatype_next_np = NULL;
	datatype_prev_np->datatype_next_np = datatype_np;
	datatype_np->datatype_info.def.print_handler = registry_print_address;
	datatype_np->datatype_info.def.store_handler = registry_store_address;
	strlcpy(datatype_np->datatype_info.tag, STM_REGISTRY_ADDRESS, (STM_REGISTRY_MAX_TAG_SIZE - 1));

	HeadDataType_removable_np = datatype_np->datatype_next_np;
	infra_os_sema_signal(&g_registry_data_sem_lock);
	return 0;
}

int registry_internal_remove_default_data_type(void)
{
	registry_datatype_t *datatype_np;
	registry_datatype_t *datatype_next_np;

	infra_os_sema_wait(&g_registry_data_sem_lock);
	datatype_np = HeadDataType_np->datatype_next_np;
	datatype_next_np = datatype_np->datatype_next_np;

	infra_os_free(datatype_np);

	datatype_np = datatype_next_np;
	datatype_next_np = datatype_np->datatype_next_np;

	infra_os_free(datatype_np);
	HeadDataType_removable_np = NULL;
	infra_os_sema_signal(&g_registry_data_sem_lock);

	return 0;
}
