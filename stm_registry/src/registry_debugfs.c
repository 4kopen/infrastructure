/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/
#include <linux/debugfs.h>
#include <linux/module.h>
#include "stm_registry.h"
#include "registry_internal.h"
static struct dentry *stm_reg_dir;
static struct dentry *stm_reg_entry;

static ssize_t registry_debugfs_dumptheregistry(struct file *f, char __user *buf, size_t count, loff_t *ppos);

static const struct file_operations registry_fops = {
	.owner = THIS_MODULE,
	.read = registry_debugfs_dumptheregistry
};

static ssize_t registry_debugfs_dumptheregistry(struct file *f, char __user *buf, size_t count, loff_t *ppos)
{
	registry_internal_dump_registry();
	return 0;
}

int registry_create_debugfs(void)
{
	stm_reg_dir = debugfs_create_dir("stm_registry", NULL);
	if (!stm_reg_dir) {
		REGISTRY_ERROR_MSG(" <%s> : <%d> Failed to create stm directory folder!\n", __FUNCTION__, __LINE__);
		return -1;
	}

	stm_reg_entry = debugfs_create_file("dump_registry", 0644, stm_reg_dir, 0, &registry_fops);
	if (!stm_reg_entry) {
		REGISTRY_ERROR_MSG("<%s> : <%d> Failed to create dentry for Dump_registry\n", __FUNCTION__, __LINE__);
		return -1;
	}
	return 0;
}

void registry_remove_debugfs(void)
{
	debugfs_remove(stm_reg_entry);
	debugfs_remove(stm_reg_dir);
}
