/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef _STM_EVENT_H
#define _STM_EVENT_H

#include "stm_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#define STM_EVT_DATA_USAGE			0
#define STM_EVT_NO_ERROR			0



typedef struct stm_event_subscription_s *stm_event_subscription_h;

typedef enum stm_event_subscription_op_e {
	STM_EVENT_SUBSCRIPTION_OP_ADD,
	STM_EVENT_SUBSCRIPTION_OP_REMOVE,
	STM_EVENT_SUBSCRIPTION_OP_UPDATE
} stm_event_subscription_op_t;

typedef struct stm_event_s {
	stm_object_h	object;
	unsigned int	event_id;
} stm_event_t;

typedef struct stm_event_info_s {
	stm_event_t	event;
	stm_time_t	timestamp;
	bool		events_missed;
	void		*cookie;
} stm_event_info_t;


typedef struct stm_event_subscription_entry_s {
	stm_object_h	object;
	unsigned int	event_mask;
	void		*cookie;
} stm_event_subscription_entry_t;

typedef void (*stm_event_handler_t)(
	unsigned int				number_of_events,
	stm_event_info_t			*events);

int __must_check stm_event_subscription_create(
					       stm_event_subscription_entry_t *subscription_entries,
					       unsigned int number_of_entries,
					       stm_event_subscription_h *subscription);

int __must_check stm_event_subscription_delete(stm_event_subscription_h subscription);

int __must_check stm_event_subscription_modify(
					       stm_event_subscription_h subscription,
					       stm_event_subscription_entry_t *subscription_entry,
					       stm_event_subscription_op_t operation);

int __must_check stm_event_wait(
				stm_event_subscription_h subscription,
				int		timeout,
				unsigned int	max_number_of_events,
				unsigned int	*number_of_events_p,
				stm_event_info_t *events);

int __must_check stm_event_set_handler(
				       stm_event_subscription_h subscription,
				       stm_event_handler_t event_handler);

int __must_check stm_event_get_handler(
				       stm_event_subscription_h subscription,
				       stm_event_handler_t *event_handler_p);

int __must_check stm_event_set_wait_queue(
					  stm_event_subscription_h subscription,
					  stm_event_wait_queue_t *queue,
					  bool	interruptible);


int __must_check stm_event_get_wait_queue(
					  stm_event_subscription_h subscription,
					  stm_event_wait_queue_t **queue_p,
					  bool	*interruptible_p);

int __must_check stm_event_signal(stm_event_t		*event);




#ifdef __cplusplus
}
#endif

#endif /*_STM_EVENT_H*/
