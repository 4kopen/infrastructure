/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef _STM_COMMON_H
#define _STM_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

typedef void			*stm_object_h;
typedef unsigned long        	stm_time_t;

#if defined (__KERNEL__)
#include "linux/compiler.h"
#else
#ifdef __GNUC__
#define __must_check __attribute__ ((warn_unused_result))
#endif
#endif

/* The Linux headers are not C++ compatible and so we must hide them from
 * C++ compilers.
 */
#if defined(__KERNEL__) && !defined(__cplusplus)
#include "linux/types.h"
#include <linux/wait.h>
typedef wait_queue_head_t			stm_event_wait_queue_t;
#else
/* Dummy definition to ensure other infrastructure headers can be parsed */
typedef void            			stm_event_wait_queue_t;
typedef unsigned int				size_t;
#endif

typedef void (*stm_error_handler) (void *ctxt, stm_object_h bad_boy);

#ifdef __cplusplus
}
#endif

#endif /*_STM_COMMON_H*/
