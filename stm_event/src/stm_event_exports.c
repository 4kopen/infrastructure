/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/*
   @file     stm_event_exports.c
   @brief    Implementation for exporting Event Manager's functions

 */


/* Requires   MODULE         defined on the command line */
/* Requires __KERNEL__       defined on the command line */
/* Requires __SMP__          defined on the command line for SMP systems   */
/* Requires EXPORT_SYMTAB    defined on the command line to export symbols */

#include <linux/init.h>    /* Initiliasation support */
#include <linux/module.h>  /* Module support */
#include <linux/kernel.h>  /* Kernel support */
#include <linux/version.h> /* Kernel version */

#include "stm_event.h"


#define EXPORT_SYMTAB

EXPORT_SYMBOL(stm_event_subscription_create);
EXPORT_SYMBOL(stm_event_subscription_delete);
EXPORT_SYMBOL(stm_event_subscription_modify);
EXPORT_SYMBOL(stm_event_wait);
EXPORT_SYMBOL(stm_event_set_handler);
EXPORT_SYMBOL(stm_event_get_handler);
EXPORT_SYMBOL(stm_event_set_wait_queue);
EXPORT_SYMBOL(stm_event_get_wait_queue);
EXPORT_SYMBOL(stm_event_signal);
