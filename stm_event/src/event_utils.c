/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include "event_utils.h"

#define EVT_MAX_BITS_IN_UINT32		32

int32_t evt_mng_get_bit_pos(uint32_t value)
{
	uint32_t bit_pos = 0;

	for (; bit_pos < EVT_MAX_BITS_IN_UINT32; bit_pos++) {
		if (value & (1 << bit_pos))
			return bit_pos;

	}

	return -1;
}
