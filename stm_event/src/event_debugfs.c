/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/*
   @file     event_debugfs.c
   @brief

 */
#include <linux/module.h>  /* Module support */
#include <linux/kernel.h>  /* Kernel support */
#include <linux/debugfs.h>

#include "event_signaler.h"
#include "event_subscriber.h"
#include "event_debugfs.h"

static struct dentry *stm_event_dir;
static struct dentry *stm_event_signaler_entry;
static struct dentry *stm_event_subscriber_entry;

static ssize_t signaler_dump_debugfs(struct file *f, char __user *buf, size_t count, loff_t *ppos);
static ssize_t subscriber_dump_debugfs(struct file *f, char __user *buf, size_t count, loff_t *ppos);
static int io_open(struct inode *inode, struct file *file);

static const struct file_operations signaler_fops = {
	.owner = THIS_MODULE,
	.read = signaler_dump_debugfs,
	.open = io_open
};

static const struct file_operations subscriber_fops = {
	.owner = THIS_MODULE,
	.read = subscriber_dump_debugfs,
	.open = io_open
};

static ssize_t signaler_dump_debugfs(struct file *f, char __user *buf, size_t count, loff_t *ppos)
{
	return dump_signaler(f, buf, count, ppos);
}


static int io_open(struct inode *inode, struct file *file)
{

	struct io_descriptor *io_desc;

	if (inode->i_private) {
		file->private_data = inode->i_private;
		io_desc = file->private_data;
		io_desc->size = 0;
		io_desc->size_allocated = 0;
	} else
		return -EFAULT;

	return 0;
}
static ssize_t subscriber_dump_debugfs(struct file *f, char __user *buf, size_t count, loff_t *ppos)
{
	return dump_subscriber(f, buf, count, ppos);
}

struct io_descriptor io_val = {
	/*.size =*/0,
	/*.size_allocated =*/0,
	/*.data =*/NULL
};
int event_create_debugfs(void)
{
	stm_event_dir = debugfs_create_dir("stm_event", NULL);
	if (!stm_event_dir) {
		pr_err(" <%s> : <%d> Failed to create stm event directory folder!\n", __FUNCTION__, __LINE__);
		return -1;
	}

	stm_event_signaler_entry = debugfs_create_file("dump_signalers", 0644, stm_event_dir, &io_val, &signaler_fops);
	if (!stm_event_signaler_entry) {
		pr_err("<%s> : <%d> Failed to create dentry for dump_signaler\n", __FUNCTION__, __LINE__);
		return -1;
	}

	stm_event_subscriber_entry = debugfs_create_file("dump_subscribers", 0644, stm_event_dir, &io_val, &subscriber_fops);
	if (!stm_event_subscriber_entry) {
		pr_err("<%s> : <%d> Failed to create dentry for dump_subscriber\n", __FUNCTION__, __LINE__);
		return -1;
	}
	return 0;
}

void event_remove_debugfs(void)
{
	debugfs_remove(stm_event_signaler_entry);
	debugfs_remove(stm_event_subscriber_entry);
	debugfs_remove(stm_event_dir);
}
