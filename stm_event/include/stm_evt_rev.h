/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/*
   @file     stm_evt_rev.h
   @brief    Contains the event manager revision string

 */

/* Define to prevent recursive inclusion */
#ifndef __EVT_REV_H
#define __EVT_REV_H

#ifdef __cplusplus
extern "C" {
#endif

#define STM_EVT_REV		"STM_EVT-REL_0.0.1"

#ifdef __cplusplus
}
#endif

#endif /* #ifndef __EVT_REV_H */

/* End of stm_evt_rev.h */
