/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef _EVENT_TYPES_H_
#define _EVENT_TYPES_H_


#define EVT_MNG_EVT_ARR_SIZE		32
#define EVT_MNG_EVT_DATA_SIZE		4 /*In word size*/
#define EVT_INAVLID_HANDLE		0xFFFFFFFF

typedef struct infra_q_s		evt_mng_subscribe_q_t;
typedef struct infra_q_s		evt_mng_signaler_info_q_t;

typedef struct evt_mng_signaler_param_s	*evt_mng_signaler_param_h;
typedef struct infra_q_s		evt_mng_signaler_q_t;

typedef struct evt_mng_check_event_param_s {
	uint8_t		*evt_info_mem_p;
	/*Set 1, if events are to be retreived.
	Set 0, for event lookup*/
	uint8_t		do_evt_retrieval;
	uint32_t	evt_occurence_mask;
	uint32_t	evt_occurence_info[EVT_MNG_EVT_ARR_SIZE];
	uint32_t	max_evt_space;
	uint32_t	num_evt_occured;
	uint32_t	requested_evt_mask;
	int32_t		timeout;
	void		*cookie;
} evt_mng_check_event_param_t;
#endif
