/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef _EVENT_SIGNALER_H
#define _EVENT_SIGNALER_H

#include "stm_event.h"
#include "infra_os_wrapper.h"
#include "infra_queue.h"
#include "event_types.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef int (*evt_mng_term_signaler_fp)(void *);

/*Control Block of each Signaler*/
typedef struct evt_mng_signaler_param_s {
	stm_object_h			object;
	uint32_t			signaler_handle;
	uint32_t			evt_mask;
	uint32_t			evt_occured;
	wait_queue_head_t		delete_waitq;
	atomic_t			use_cnt;
	infra_os_rwlock_t		lock_signaler_node;
	/*Head of the Subscriber Linked List*/
	evt_mng_subscribe_q_t		*subcriber_head_p;
	/*Node Memory to create the Linked List*/
	evt_mng_signaler_q_t		signaler_q_p;
} evt_mng_signaler_param_t;



typedef struct evt_mng_signaler_control_s {
	evt_mng_signaler_q_t		*head;
	uint32_t			num_signaler;
	infra_os_rwlock_t		lock_sig_q_param_p;
} evt_mng_signaler_control_t;

void evt_mng_set_signaler_control_param(evt_mng_signaler_control_t *control_p);
evt_mng_signaler_control_t *evt_mng_get_signaler_control_param(void);

stm_object_h evt_mng_get_signaler_id(uint32_t signaler_handle);

infra_error_code_t evt_mng_add_signaler(stm_event_subscription_entry_t *subscription_entry,
					uint32_t *signaler_handle_p);

infra_error_code_t evt_mng_signaler_deattach_subscriber(uint32_t signaler_handle,
							void *subscriber_p);

infra_error_code_t evt_mng_signaler_update_subscriber_info(stm_event_subscription_entry_t *subscription_entry,
							   uint32_t signaler_handle);

infra_error_code_t evt_mng_signaler_attach_subscriber(stm_event_subscription_entry_t *subscription_entry,
						      uint32_t *signaler_handle_p,
						      void *subscriber_p);

infra_error_code_t evt_mng_handle_evt(stm_event_t *event);


ssize_t dump_signaler(struct file *f, char __user *buf, size_t count, loff_t *ppos);

#ifdef __cplusplus
}
#endif

#endif /*_EVENT_SIGNALER_H*/
