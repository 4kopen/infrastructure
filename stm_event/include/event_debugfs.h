/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/*
   @file     event_debugfs.h
   @brief

 */

#ifndef _EVENT_DEBUGFS_H
#define _EVENT_DEBUGFS_H

#if (defined SDK2_EVENT_ENABLE_DEBUGFS_STATISTICS)

#ifdef __cplusplus
extern "C" {
#endif

struct io_descriptor {
	size_t size;
	size_t size_allocated;
	void *data;
};

int event_create_debugfs(void);
void event_remove_debugfs(void);

#ifdef __cplusplus
}
#endif

#endif
#endif	/* _EVENT_DEBUGFS_H */
