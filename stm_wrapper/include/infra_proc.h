/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/*
   @file     stttx_core_main.c
   @brief

 */

#ifndef INFRA_PROC_H_
#define INFRA_PROC_H_

#include "infra_proc_interface.h"

typedef struct infra_proc_control_param_t{
	uint32_t				max_dir_level;
	infra_proc_dir_name_t		dir_name[INFRA_MAX_PROC_DIR_ID];
	infra_proc_entry_t			proc_entry_p[INFRA_MAX_PROC_DIR_ID];
	/*pointer to the mesage received from the user will be copied*/
	uint8_t				*msg_buffer_p;
	uint32_t				msg_max_buffer_size;
	infra_proc_read_fp			read_fp;
	infra_proc_cmd_fp			write_fp;
	uint32_t				handle;
}infra_proc_control_param_t;

int	infra_term_this_proc(void* );
void 	*infra_init_this_proc(infra_proc_create_param_t*, infra_proc_entry_t);

#endif
