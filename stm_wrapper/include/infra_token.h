/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/
#ifndef INFRA_TOKENIZER_H_
#define INFRA_TOKENIZER_H_

typedef enum{
	UNKNOWN = 0,
	STRING = 1,
	INTEGER = 2,
	HEX = 3
}infra_type_t;

char *get_tc_name(char *buf);

int check_for_string (char *buf, void *data_p, uint8_t size);

int check_for_integer (char *buf, void *data_p);

int get_arg(void **data_p,infra_type_t type , void *argument, uint8_t arg_size);

int get_arg_size(char *data_p);

int get_number_of_tokens(char *data_p);

//infra_error_code_t get_param_data(char *param_str, void *data_p);

#endif
