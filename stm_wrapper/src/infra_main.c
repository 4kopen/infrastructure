/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

/*
   @file     infra_main.c
   @brief

 */


/* Requires   MODULE         defined on the command line */
/* Requires __KERNEL__       defined on the command line */
/* Requires __SMP__          defined on the command line for SMP systems   */
/* Requires EXPORT_SYMTAB    defined on the command line to export symbols */

#include <linux/init.h>    /* Initiliasation support */
#include <linux/module.h>  /* Module support */
#include <linux/kernel.h>  /* Kernel support */

#include "infra_queue.h"
#include "infra_os_wrapper.h"
#include "infra_proc_interface.h"

/*** MODULE INFO *************************************************************/

MODULE_AUTHOR("Bharat Jauhari");
MODULE_DESCRIPTION("OS WRAPPER FOR INFRA MODULE");
MODULE_SUPPORTED_DEVICE("");
MODULE_LICENSE("GPL");


/*** PROTOTYPES **************************************************************/

static int  infra_main_entry_module(void);
static void infra_main_exit_module(void);

/*** MODULE PARAMETERS *******************************************************/

/*** GLOBAL VARIABLES *********************************************************/


/*** EXPORTED SYMBOLS ********************************************************/

/*** LOCAL TYPES *********************************************************/


/*** METHODS ****************************************************************/

/*=============================================================================

   infra_main_entry_module

   Initialise the module (initialise the access routines (fops) and set the
   major number. Aquire any resources needed and setup internal structures.

  ===========================================================================*/
static int __init infra_main_entry_module(void)
{
	pr_info("Load module infra_main [?]\t\tby %s (pid %i)\n", current->comm,
			current->pid);
	infra_init_procfs_module();
	return (0);  /* If we get here then we have succeeded */
}


/*=============================================================================

   infra_main_exit_module

   Realease any resources allocaed to this module before the module is
   unloaded.

  ===========================================================================*/
static void __exit infra_main_exit_module(void)
{
	infra_term_procfs_module();
	pr_info("Unload module infra_os_wrapper by %s (pid %i)\n",
			current->comm, current->pid);
}

/*** MODULE LOADING ******************************************************/

/* Tell the module system these are our entry points. */

module_init(infra_main_entry_module);
module_exit(infra_main_exit_module);
