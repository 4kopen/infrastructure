/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/
#ifndef __MEM_UTIL_H_
#define __MEM_UTIL_H_

#include <linux/init.h>
#include <linux/module.h>
#include <linux/pagemap.h>
#include <linux/scatterlist.h>
#include <linux/bpa2.h>

#include "stm_data_interface.h"
#include "infra_os_wrapper.h"

#define INVALIDATE	1
#define NO_INVALIDATE	0
#define DVB_TS_PKT_SZ 188
#define MAX_BYTE_PER_PAGE ((PAGE_SIZE/DVB_TS_PKT_SZ)*DVB_TS_PKT_SZ)

int __init stm_memsrc_init(void);
void __exit stm_memsrc_term(void);

int __init stm_memsink_init(void);
void __exit stm_memsink_term(void);

int generate_block_list(char *buf, int32_t count,
			struct scatterlist **input_sg_list,
			struct page ***input_pages,
			struct stm_data_block **input_block_list,
			uint32_t *pages_mapped);

void free_block_list(struct page **pages,
		     struct scatterlist *sg_list,
		     struct stm_data_block *input_block_list,
		     uint32_t mapped,
		     uint32_t filled_blocks);

int generate_block_list_vmalloc(char *buf, int32_t count,
				struct scatterlist **input_sg_list,
				struct page ***input_pages,
				struct stm_data_block **input_block_list,
				uint32_t *pages_mapped);

void free_block_list_vmalloc(struct page **pages,
			     struct scatterlist *sg_list,
			     struct stm_data_block *input_block_list,
			     uint32_t mapped,
			     uint32_t filled_blocks);

int get_user_block_list(struct stm_data_block *input_block_list,
			     struct scatterlist **input_sg_list,
			     struct page ***input_pages,
			     struct stm_data_block **generated_block_list,
			     int *mapped, bool *region_is_bpa2);

void free_user_block_list(struct page **pages,
			     struct scatterlist *sg_list,
			     struct stm_data_block *input_block_list,
			     uint32_t mapped,
			     uint32_t filled_blocks,
			     bool region_is_bpa2);

#endif /* __MEM_UTIL_H_ */
