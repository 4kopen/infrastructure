/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/
#include <linux/init.h>
#include <linux/module.h>

#include "memutil.h"
#include "infra_os_wrapper.h"
#include "stm_registry.h"
#include "stm_event.h"
#include "stm_memsrc.h"
#include "stm_data_interface.h"
#include "mss_debug.h"

MODULE_AUTHOR("STMicroelectronics");
MODULE_DESCRIPTION("Module for memory source");
MODULE_LICENSE("GPL");

#define src_key		0xbadabdc7
#define ENOTSUP EOPNOTSUPP

typedef enum stm_memsrc_state_e {
	STM_MEMSRC_STATE_DETACHED,
	STM_MEMSRC_STATE_CHECK_CONNECTION,
	STM_MEMSRC_STATE_READY,
	STM_MEMSRC_STATE_PROCESSING,
	STM_MEMSRC_STATE_REPORTING,
	STM_MEMSRC_STATE_EXIT
} stm_memsrc_state_t;

struct stm_memsrc_class_s {
	stm_object_h myself;
};

static struct stm_memsrc_class_s memsrc_type = {0};

static int memsrc_update_state(struct stm_memsrc_s *memsrc, stm_memsrc_state_t state);

static int handle_push_in_kernel_mode(struct stm_memsrc_s *src_p,
				      struct stm_data_block *block_list,
				      uint32_t *injected_size);
static int handle_push_in_kernel_vmalloc_mode(struct stm_memsrc_s *src_p,
					      struct stm_data_block *block_list,
					      uint32_t *injected_size);
static int handle_push_in_user_mode(struct stm_memsrc_s *src_p,
				    struct stm_data_block *block_list,
				    uint32_t *injected_size);
static int memsrc_check_state(struct stm_memsrc_s *src_p,
			      stm_memsrc_state_t desired_state);
static int stm_memsrc_notify(stm_object_h src, unsigned int event_id);
static int handle_push_in_kernel_mode_mdata(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size);
static int handle_push_in_user_mode_mdata(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size);
static int handle_push_in_user_mode_mdata_secure(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size);
static int handle_push_in_kernel_vmalloc_mode_mdata(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size);
static int handle_push_in_kernel_vmalloc_mode_mdata_secure(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size);

/*! Memory source object */
struct stm_memsrc_s {
	uint32_t key;
	stm_memory_iomode_t mode;
	stm_object_h attach;
	stm_data_interface_push_sink_t sink_interface;
	stm_data_interface_push_notify_t notify_interface;
	stm_data_mem_type_t address_space;
	stm_memsrc_state_t	cur_state;
	infra_os_mutex_t state_mutex;
	bool path_security_is_set;
	bool path_is_secure;
};

stm_data_interface_push_notify_t mem_src_interface_push_notify = {
	.notify = stm_memsrc_notify,
};

static int memsrc_update_state(struct stm_memsrc_s *memsrc, stm_memsrc_state_t state){
	int ret = 0;
	ret = infra_os_mutex_lock(&memsrc->state_mutex);
	if (ret < 0) {
		return ret;
	}
	memsrc->cur_state = state;
	infra_os_mutex_unlock(&memsrc->state_mutex);
	return ret;
}


static bool memsrc_obj_is_valid(struct stm_memsrc_s *memsrc_obj_p)
{
	bool	ret = true;
	if (memsrc_obj_p == NULL || memsrc_obj_p->key != src_key)
		ret = false;
	return ret;
}

int __init stm_memsrc_init(void)
{
	int		error = 0;

/* register the memory source object type */
	error = stm_registry_add_object(STM_REGISTRY_TYPES,
		      "memsource",
		      (stm_object_h) &memsrc_type);
	if (error)
		MSS_ERROR_MSG("stm_regstry_add_object failed(%d)\n", error);

	memsrc_type.myself = (stm_object_h) &memsrc_type;

	error = stm_registry_add_attribute(memsrc_type.myself, /*parent*/
		      STM_DATA_INTERFACE_PUSH_NOTIFY, /*tag*/
		      STM_REGISTRY_ADDRESS, /*data type tag*/
		      &mem_src_interface_push_notify, /*value*/
		      sizeof(mem_src_interface_push_notify)); /*value size*/
	if (error)
		MSS_ERROR_MSG("stm_regstry_add_attribute failed(%d)\n", error);

	return error;
}

void __exit stm_memsrc_term(void)
{
	int		error = 0;
	error = stm_registry_remove_object((stm_object_h) &memsrc_type);
	if (error)
		MSS_ERROR_MSG("stm_registry_remove_object failed(%d)\n",
			      error);

	error = stm_registry_remove_attribute(memsrc_type.myself,
		      STM_DATA_INTERFACE_PUSH_NOTIFY);
	if (error)
		MSS_ERROR_MSG("stm_registry_remove_attribute failed(%d)\n",
			      error);
}

/*!

*/
int
stm_memsrc_new(const char *name,
	       stm_memory_iomode_t iomode,
	       stm_data_mem_type_t address_space,
	       stm_memsrc_h *memsrc_ctx)
{
	struct stm_memsrc_s *memsrc_obj_p;
	int retval;

	if ((!memsrc_ctx) || (!name))
		return -EINVAL;

	if (address_space > KERNEL || address_space < USER)
		return -EINVAL;

	memsrc_obj_p = infra_os_malloc(sizeof(*memsrc_obj_p));
	if (!memsrc_obj_p)
		return -ENOMEM;

	/* infra_os_malloc already memset the buffer to 0 */
	retval = stm_registry_add_instance(STM_REGISTRY_INSTANCES, /*parent*/
		      memsrc_type.myself, /*type*/
		      name,
		      (stm_object_h) memsrc_obj_p);
	if (retval) {
		/* error in the registry */
		MSS_ERROR_MSG("stm_registry_add_instance failed(%d)\n",
			      retval);
		infra_os_free(memsrc_obj_p);
		*memsrc_ctx = NULL;
		return retval;
	}

	/* not usefull as long as infra_os_malloc set to 0 buffers */
	memsrc_obj_p->attach = (stm_object_h) NULL; /* Not yet attached */
	memset(&(memsrc_obj_p->sink_interface),
		      0,
		      sizeof(memsrc_obj_p->sink_interface));

	memset(&(memsrc_obj_p->notify_interface),
		      0, sizeof(memsrc_obj_p->notify_interface));
	retval = infra_os_mutex_initialize(&(memsrc_obj_p->state_mutex));
	if (retval != INFRA_NO_ERROR)
		return retval;
	memsrc_obj_p->cur_state = STM_MEMSRC_STATE_DETACHED;
	memsrc_obj_p->address_space = address_space;
	memsrc_obj_p->key = src_key;
	memsrc_obj_p->mode = iomode;
	memsrc_obj_p->path_security_is_set = false;
	memsrc_obj_p->path_is_secure  = false;
	*memsrc_ctx = memsrc_obj_p;

	return 0;
}
EXPORT_SYMBOL(stm_memsrc_new);

/*!

*/
int
stm_memsrc_delete(stm_memsrc_h memsrc_ctx)
{
	struct stm_memsrc_s *memsrc_obj_p = memsrc_ctx;
	int ret = 0;

	MSS_DEBUG_MSG(MEM_SRC, "(%p)\n", memsrc_ctx);
	if (!memsrc_obj_is_valid(memsrc_obj_p))
		return -ENXIO;

	ret = memsrc_check_state(memsrc_obj_p, STM_MEMSRC_STATE_EXIT);
	if (ret < 0)
		return ret;

	ret = stm_registry_remove_object(memsrc_ctx);
	if (ret)
		MSS_ERROR_MSG("stm_registry_remove_object(:%d)\n", ret);

	infra_os_mutex_terminate(&memsrc_obj_p->state_mutex);
	memsrc_obj_p->key = 0x0;
	memsrc_obj_p->attach = NULL;
	infra_os_free(memsrc_obj_p);

	return 0;
}
EXPORT_SYMBOL(stm_memsrc_delete);

/*!

 */
int
stm_memsrc_attach(stm_memsrc_h memsrc_ctx,
		  stm_object_h consumer_ctx,
		  const char *consumer_interface_type)
{
	int retval = 0;
	stm_object_h consumer_type;
	uint32_t returned_size = 0;
	struct stm_memsrc_s *memsrc_obj_p = memsrc_ctx;
        char tagTypeName [STM_REGISTRY_MAX_TAG_SIZE];


/* TODO check if consumer_ctx is a valid object to connect to */
	if (!memsrc_obj_is_valid(memsrc_obj_p))
		return -ENXIO;

	/*get the source object type*/
	retval = stm_registry_get_object_type(consumer_ctx, &consumer_type);
	if (retval) {
		MSS_ERROR_MSG("stm_registry_get_object_type(%p, &%p)",
			      consumer_ctx,
			      consumer_type);
		return -ENODEV;
	}

	retval = stm_registry_get_attribute(consumer_type,
		      consumer_interface_type,
		      tagTypeName,
		      sizeof(memsrc_obj_p->sink_interface),
		      &memsrc_obj_p->sink_interface,
		      &returned_size);

	if (retval) {
		if ((retval == -ENOMEM) ||
			(returned_size != sizeof(memsrc_obj_p->sink_interface))) {
			MSS_ERROR_MSG("error attribute not compatible\n");
			return -ECONNREFUSED;
		} else {
			MSS_ERROR_MSG("doesn't recognize consumer\n");
			return -ENODEV;
		}
	}

	retval = memsrc_check_state(memsrc_obj_p,
		      STM_MEMSRC_STATE_CHECK_CONNECTION);
	if (retval < 0)
		return retval;

	memsrc_obj_p->sink_interface.mode = memsrc_obj_p->mode;

	if (0 != stm_registry_add_attribute(consumer_ctx, /* parent */
		consumer_interface_type, /*tag*/
		STM_REGISTRY_ADDRESS, /* data type tag*/
		&memsrc_obj_p->sink_interface,
		sizeof(memsrc_obj_p->sink_interface)))
		MSS_DEBUG_MSG(MEM_SRC, "attribute already exists\n");

	/* call the sink interface's connect handler to connect the consumer */
	retval = memsrc_obj_p->sink_interface.connect(memsrc_ctx, consumer_ctx);
	if (retval) {
		MSS_ERROR_MSG("connect callback failed (%d)\n",
			      retval);
		retval = memsrc_update_state(memsrc_obj_p,
			      STM_MEMSRC_STATE_DETACHED);
		return -ECONNREFUSED;
	}

	retval = stm_registry_add_connection(memsrc_ctx,
		      "attach",
		      consumer_ctx);

	if (!retval)
		memsrc_obj_p->attach = consumer_ctx;

	retval = memsrc_update_state(memsrc_obj_p, STM_MEMSRC_STATE_READY);

	return retval;
}
EXPORT_SYMBOL(stm_memsrc_attach);

/*!

*/
int
stm_memsrc_detach(stm_memsrc_h memsrc_ctx)
/* a second parameter [stm_object_h consumer_ctx] may be usefull
* for an other than memeory sourcerc
* but as memsrc has only one attach, useless here */
{
	int retval = 0;
	struct stm_memsrc_s *memsrc_obj_p = memsrc_ctx;
	stm_object_h consumer_ctx;

	MSS_DEBUG_MSG(MEM_SRC, "(%p)\n", memsrc_ctx);
	if (!memsrc_obj_is_valid(memsrc_obj_p)) {
		MSS_ERROR_MSG("not expected memsrc_ctx\n");
		return -ENXIO;
	}

	if (memsrc_obj_p->attach == NULL) {
		MSS_ERROR_MSG("not attachd to any object. %p, %d",
			      memsrc_obj_p,
			      retval);
		return -ENXIO;
	}

	retval = memsrc_check_state(memsrc_obj_p,
		      STM_MEMSRC_STATE_CHECK_CONNECTION);
	if (retval < 0) {
		MSS_ERROR_MSG("Detach %p, %d\n",
			      memsrc_obj_p,
			      retval);
		return retval;
	}

	consumer_ctx = memsrc_obj_p->attach;
	memsrc_obj_p->attach = NULL;

	retval = stm_registry_remove_connection(memsrc_ctx, "attach");
	if (retval) {
		MSS_ERROR_MSG("stm_registry_remove_connection(%p)fail %d\n",
			      memsrc_obj_p,
			      retval);
	}
	retval = memsrc_obj_p->sink_interface.disconnect(
		      (stm_object_h) memsrc_obj_p,
		      consumer_ctx);
	if (retval) {
		MSS_ERROR_MSG("(%p)->sink_interface.disconnect(%p) fail\n",
			      memsrc_obj_p,
			      consumer_ctx);
	}
	retval = stm_registry_remove_attribute(consumer_ctx,
		STM_DATA_INTERFACE_PUSH);
	if (retval) {
		MSS_ERROR_MSG("(%p) remove_attribute fail\n", consumer_ctx);
	}

	/* reset get_attribute */
	memset(&(memsrc_obj_p->sink_interface), 0,
		      sizeof(memsrc_obj_p->sink_interface));

	retval = memsrc_update_state(memsrc_obj_p, STM_MEMSRC_STATE_DETACHED);

	memsrc_obj_p->path_security_is_set = false;
	memsrc_obj_p->path_is_secure  = false;

	return retval;
}
EXPORT_SYMBOL(stm_memsrc_detach);

/*!

*/
int
stm_memsrc_set_control(stm_memsrc_h memsrc_ctx,
		       stm_memsrc_control_t selector,
		       uint32_t value)
{
	int retval;
	struct stm_memsrc_s *memsrc_obj_p = memsrc_ctx;

	if (!memsrc_obj_is_valid(memsrc_obj_p))
		return -ENXIO;

	switch (selector) {
	case STM_MEMSRC_FLUSH_SHARED_POOL:
		retval = -ENOSYS;
		break;
	case STM_MEMSRC_RETURN_ALL_EMPTY_BLOCKS:
		retval = -ENOSYS;
		break;
	default:
		retval = -EINVAL;
		break;
	}
	return retval;
}
EXPORT_SYMBOL(stm_memsrc_set_control);

int
stm_memsrc_set_compound_control(stm_memsrc_h memsrc_ctx,
				stm_memsrc_control_t selector,
				const void *value,
				uint32_t size)
{
	return 0;
}
EXPORT_SYMBOL(stm_memsrc_set_compound_control);

/*!

*/
int
stm_memsrc_push_data(stm_memsrc_h memsrc_ctx,
		     void *data_addr,
		     uint32_t data_length,
		     uint32_t *injected_size)
{
	struct stm_data_block block_list;
	struct stm_memsrc_s *memsrc_obj_p = memsrc_ctx;
	int retval = 0, push_err = 0;

	if (!memsrc_obj_is_valid(memsrc_obj_p)) {
		MSS_ERROR_MSG("Object non valid\n");
		return -ENXIO;
	}
	if ((!data_addr) || (!injected_size)) {
		MSS_ERROR_MSG("Data non valid\n");
		return -EINVAL;
	}

	MSS_DEBUG_MSG(MEM_SRC, "buf=%p len=%d inj=%d\n",
		      data_addr,
		      data_length,
		      *injected_size);

	retval = memsrc_check_state(memsrc_obj_p, STM_MEMSRC_STATE_PROCESSING);
	if (retval < 0)
		return retval;

	*injected_size = 0;
	block_list.data_addr = data_addr;

	/* Comply with alignment request from sink object */
	if (memsrc_obj_p->sink_interface.alignment)
		data_length &= ~((memsrc_obj_p)->sink_interface.alignment - 1);

	block_list.len = data_length;

	if (memsrc_obj_p->sink_interface.push_data) {
		switch (memsrc_obj_p->address_space) {
		case USER:
			push_err = handle_push_in_user_mode(memsrc_obj_p,
				      &block_list,
				      injected_size);
			break;

		case KERNEL_VMALLOC:
			push_err = handle_push_in_kernel_vmalloc_mode(
				      memsrc_obj_p,
				      &block_list,
				      injected_size);
			break;

		case KERNEL:
			push_err = handle_push_in_kernel_mode(memsrc_obj_p,
				      &block_list,
				      injected_size);
			break;

		case PHYSICAL:
			retval = memsrc_update_state(memsrc_obj_p,
				      STM_MEMSRC_STATE_READY);
			return -EINVAL;
		}

	} else {
		MSS_ERROR_MSG("Not a good interface\n");
		retval = -ENXIO;
	}

	MSS_DEBUG_MSG(MEM_SRC, "buf=%p len=%d inj=%d\n",
		      data_addr,
		      data_length,
		      *injected_size);
	retval = memsrc_update_state(memsrc_obj_p, STM_MEMSRC_STATE_READY);

	if (push_err < 0)
		retval = push_err;

	return retval;
}
EXPORT_SYMBOL(stm_memsrc_push_data);

static int handle_push_in_user_mode(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *block_list,
				    uint32_t *injected_size)
{
	int32_t			retval;
	uint32_t		processed = 0, mapped = 0;
	struct scatterlist	*input_sg_list;
	struct page		**input_pages;
	struct stm_data_block	*input_block_list;
	uint32_t		i = 0, temp = 0;
	bool region_is_bpa2;

	retval = get_user_block_list(block_list,
		     &input_sg_list,
		     &input_pages,
		     &input_block_list,
		     &mapped,
		     &region_is_bpa2);
	if (retval < 0)
		return retval;

	retval = memsrc_obj_p->sink_interface.push_data(memsrc_obj_p->attach,
		      input_block_list,
		      mapped,
		      &processed);

	if (region_is_bpa2)
		*injected_size = (processed == 1) ? block_list->len : 0;
	else {
		temp = processed;
		while (temp--) {
			*injected_size += input_block_list[i].len;
			i++;
		}
	}

	free_user_block_list(input_pages,
		      input_sg_list,
		      input_block_list,
		      mapped,
		      processed,
		      region_is_bpa2);

	return retval;
}

static int handle_push_in_kernel_vmalloc_mode(struct stm_memsrc_s *memsrc_obj_p,
					      struct stm_data_block *block_list,
					      uint32_t *injected_size)
{
	int32_t			retval;
	uint32_t		processed = 0, mapped = 0;
	struct scatterlist	*input_sg_list;
	struct page		**input_pages;
	struct stm_data_block	*input_block_list;
	uint32_t		i = 0, temp = 0;

	retval = generate_block_list_vmalloc(block_list->data_addr,
		      block_list->len,
		      &input_sg_list,
		      &input_pages,
		      &input_block_list,
		      &mapped);
	if (0 > retval)
		return -ENOMEM;

	retval = memsrc_obj_p->sink_interface.push_data(memsrc_obj_p->attach,
		      input_block_list,
		      mapped,
		      &processed);

	temp = processed;
	while (temp--) {
		*injected_size += input_block_list[i].len;
		i++;
	}

	free_block_list_vmalloc(input_pages,
		      input_sg_list,
		      input_block_list,
		      mapped,
		      processed);

	return retval;
}

static int __convert_chunk_to_pages(struct stm_data_block *chunk,
		struct stm_data_block *blk_list, uint32_t blk_cnt)
{
	uint32_t i = 0, cur_sz, sz;
	uint8_t *cur_src;

	cur_src = chunk->data_addr;
	cur_sz = chunk->len;
	for (; i < blk_cnt; i++) {
		sz = (cur_sz < MAX_BYTE_PER_PAGE) ? cur_sz : MAX_BYTE_PER_PAGE;
		blk_list[i].data_addr = cur_src;
		blk_list[i].len = sz;
		cur_src += sz;
		cur_sz -= sz;
	}
	if (cur_sz != 0)
		pr_warn("The current size should be zero\n");

	return 0;
}

static int handle_push_in_kernel_mode(struct stm_memsrc_s *src_p,
				      struct stm_data_block *block_list,
				      uint32_t *injected_size)
{
	int32_t		ret;
	uint32_t	processed = 0;
	uint32_t blk_cnt = 0, i = 0;
	struct stm_data_block *temp_list;

	blk_cnt = (block_list->len + MAX_BYTE_PER_PAGE - 1)/MAX_BYTE_PER_PAGE;

	temp_list = kzalloc(sizeof(struct stm_data_block) * blk_cnt, GFP_KERNEL);
	if (!temp_list)
		return -ENOMEM;

	__convert_chunk_to_pages(block_list, temp_list, blk_cnt);

	ret = src_p->sink_interface.push_data(src_p->attach,
			temp_list,
			blk_cnt,
			&processed);

	for (; i < processed; i++) {
		*injected_size += temp_list[i].len;
	}

	kfree(temp_list);

	return ret;
}

static int memsrc_check_state(struct stm_memsrc_s *src_p,
			      stm_memsrc_state_t desired_state)
{
	int ret = 0;
	stm_memsrc_state_t	cur_state;

	ret  = infra_os_mutex_lock(&src_p->state_mutex);
	if (ret < 0) {
		return ret;
	}

	cur_state = src_p->cur_state;

	switch (desired_state) {
	case STM_MEMSRC_STATE_REPORTING:
		if ((cur_state != STM_MEMSRC_STATE_READY) &&
			(cur_state != STM_MEMSRC_STATE_REPORTING)) {
			ret = -EPERM;
		}
		break;
	case STM_MEMSRC_STATE_PROCESSING:
		if (cur_state != STM_MEMSRC_STATE_READY) {
			if (cur_state == STM_MEMSRC_STATE_REPORTING)
				ret = -EBUSY;
			else
				ret = -EPERM;
		}
		break;
	case STM_MEMSRC_STATE_CHECK_CONNECTION:
		if ((cur_state != STM_MEMSRC_STATE_READY) &&
			(cur_state != STM_MEMSRC_STATE_DETACHED)) {
			if (cur_state == STM_MEMSRC_STATE_REPORTING)
				ret = -EBUSY;
			else
				ret = -EPERM;
		}
		break;
	case STM_MEMSRC_STATE_READY:
		if ((cur_state == STM_MEMSRC_STATE_EXIT) ||
			(cur_state == STM_MEMSRC_STATE_DETACHED))
			ret = -EPERM;

		break;
	case STM_MEMSRC_STATE_DETACHED:
		if ((cur_state != STM_MEMSRC_STATE_CHECK_CONNECTION))
			ret = -EPERM;

		break;
	case STM_MEMSRC_STATE_EXIT:
		if ((cur_state != STM_MEMSRC_STATE_DETACHED)) {
			if (cur_state == STM_MEMSRC_STATE_CHECK_CONNECTION)
				ret = -EBUSY;
			else
				ret = -EPERM;
		}
		break;
	}

	if (!ret)
		src_p->cur_state = desired_state;

	infra_os_mutex_unlock(&src_p->state_mutex);

	return ret;
}

int stm_memsrc_notify(stm_object_h src, unsigned int event_id)
{
	struct stm_memsrc_s *memsrc_obj_p = src;
	int error = 0;
	stm_event_t src_event;

	MSS_DEBUG_MSG(MEM_SRC, "memsrc(%p)\n", src);
	if (!memsrc_obj_is_valid(memsrc_obj_p)) {
		MSS_ERROR_MSG("not expected memsrc_ctx %p\n", src);
		return -ENXIO;
	}

	/*TODO : Add check for correct state . Remove complex state
	* machine and use only two states ATTACHED and DETACHED */

	switch (event_id) {
	case STM_MEMSRC_EVENT_CONTINUE_INJECTION:
	case STM_MEMSRC_EVENT_BUFFER_UNDERFLOW:
		src_event.event_id = event_id;
		break;

	default:
		MSS_ERROR_MSG("Not expected event Id : %d\n", event_id);
		error = -ENXIO;
	}

	if (!error) {
		src_event.object = src;
		MSS_DEBUG_MSG(MEM_SRC, "signal %d for %p\n",
			      event_id, src_event.object);
		error = stm_event_signal(&src_event);
	}
	return error;
}

int stm_memsrc_set_path_security(stm_memsrc_h memsrc_ctx, bool path_is_secure)
{
	struct stm_memsrc_s *memsrc_obj_p = memsrc_ctx;
	int error = 0;
	int error1 = 0;

	MSS_DEBUG_MSG(MEM_SRC, "memsrc(%p)\n", memsrc_obj_p);
	if (!memsrc_obj_is_valid(memsrc_obj_p)) {
		MSS_ERROR_MSG("not expected memsrc_ctx %p\n", memsrc_obj_p);
		error = -ENXIO;
		goto error_return;
	}

	/* check memsrc object connected to some sink object */
	error = memsrc_check_state(memsrc_obj_p, STM_MEMSRC_STATE_READY);
	if (error < 0) {
		MSS_ERROR_MSG("memsrc_ctx %p not not connected, state %d\n",
				     memsrc_obj_p,
				     memsrc_obj_p->cur_state);
		goto error_return;
	}

	if (!memsrc_obj_p->sink_interface.set_secure_path_status && !memsrc_obj_p->sink_interface.set_path_security) {
		MSS_ERROR_MSG("set_path_security/set_secure_path_status not set by sink for memsrc_ctx %p\n",
				     memsrc_obj_p);
		error = -ENXIO;
		goto error_return;
	}

	if (memsrc_obj_p->sink_interface.set_secure_path_status)
		error1 = memsrc_obj_p->sink_interface.set_secure_path_status(memsrc_obj_p->attach, path_is_secure);

	if (memsrc_obj_p->sink_interface.set_path_security)
		error = memsrc_obj_p->sink_interface.set_path_security(memsrc_obj_p->attach, path_is_secure);

	if (!error || !error1) {
		memsrc_obj_p->path_security_is_set = true;
		memsrc_obj_p->path_is_secure = path_is_secure;
	}

error_return:
	return error;
}
EXPORT_SYMBOL(stm_memsrc_set_path_security);

int stm_memsrc_push_data_mdata(stm_memsrc_h memsrc_ctx, void *data_addr, uint32_t data_length, uint32_t *injected_data_size,
				     void *mdata_addr, uint32_t mdata_length, uint32_t *injected_mdata_size)
{
	struct stm_memsrc_s *memsrc_obj_p = memsrc_ctx;
	int error = 0, push_err = 0;
	struct stm_data_block data_block_list;
	struct stm_data_block mdata_block_list;

	MSS_DEBUG_MSG(MEM_SRC, "memsrc(%p)\n", memsrc_ctx);
	if (!memsrc_obj_is_valid(memsrc_obj_p)) {
		MSS_ERROR_MSG("not expected memsrc_ctx %p\n", memsrc_ctx);
		error = -ENXIO;
		goto error_return;
	}

	if (!memsrc_obj_p->path_security_is_set) {
		MSS_ERROR_MSG("stm_memsrc_set_path_security not set for memsrc_ctx %p\n", memsrc_ctx);
		error = -EPERM;
		goto error_return;
	}

	if (memsrc_obj_p->mode != STM_IOMODE_BLOCKING_IO) {
		/* This function is supported only for STM_IOMODE_BLOCKING_IO mode */
		MSS_ERROR_MSG("IO mode %d not allowed for memsrc_ctx %p\n",
				     memsrc_obj_p->mode,
				     memsrc_obj_p);
		error = -ENOTSUP;
		goto error_return;
	}

	if (!memsrc_obj_p->sink_interface.push_data_mdata) {
		MSS_ERROR_MSG("push_data_mdata null not allowed for memsrc_ctx %p\n",
				     memsrc_obj_p);
		error = -ENOTSUP;
		goto error_return;
	}

	error = memsrc_check_state(memsrc_obj_p, STM_MEMSRC_STATE_PROCESSING);
	if (error < 0)
		goto error_return;

	MSS_DEBUG_MSG(MEM_SRC, "data buf=%p len=%d inj=%d\n",
		      data_addr,
		      data_length,
		      *injected_data_size);

	MSS_DEBUG_MSG(MEM_SRC, "mdata buf=%p len=%d inj=%d\n",
		      mdata_addr,
		      mdata_length,
		      *injected_mdata_size);

	*injected_data_size = 0;
	data_block_list.data_addr = data_addr;
	data_block_list.len = data_length;

	*injected_mdata_size = 0;
	mdata_block_list.data_addr = mdata_addr;
	mdata_block_list.len = mdata_length;

	if (memsrc_obj_p->sink_interface.push_data_mdata) {
		/* in case of non-secure, both data and mdata have same address space */
		if (memsrc_obj_p->path_is_secure) {
			switch (memsrc_obj_p->address_space) {
			case USER:
				push_err = handle_push_in_user_mode_mdata_secure(memsrc_obj_p,
					      &data_block_list,
					      injected_data_size,
					      &mdata_block_list,
					      injected_mdata_size);
				break;

			case KERNEL_VMALLOC:
				push_err = handle_push_in_kernel_vmalloc_mode_mdata_secure(memsrc_obj_p,
					      &data_block_list,
					      injected_data_size,
					      &mdata_block_list,
					      injected_mdata_size);
				break;

			case KERNEL:
				push_err = handle_push_in_kernel_mode_mdata(memsrc_obj_p,
					      &data_block_list,
					      injected_data_size,
					      &mdata_block_list,
					      injected_mdata_size);
				break;

			case PHYSICAL:
				memsrc_update_state(memsrc_obj_p,
					      STM_MEMSRC_STATE_READY);
				error = -EINVAL;
				goto error_return;
			}
		} else {
			switch (memsrc_obj_p->address_space) {
			case USER:
				push_err = handle_push_in_user_mode_mdata(memsrc_obj_p,
					      &data_block_list,
					      injected_data_size,
					      &mdata_block_list,
					      injected_mdata_size);
				break;

			case KERNEL_VMALLOC:
				push_err = handle_push_in_kernel_vmalloc_mode_mdata(memsrc_obj_p,
					      &data_block_list,
					      injected_data_size,
					      &mdata_block_list,
					      injected_mdata_size);
				break;

			case KERNEL:
				push_err = handle_push_in_kernel_mode_mdata(memsrc_obj_p,
					      &data_block_list,
					      injected_data_size,
					      &mdata_block_list,
					      injected_mdata_size);
				break;

			case PHYSICAL:
				memsrc_update_state(memsrc_obj_p,
					      STM_MEMSRC_STATE_READY);
				error = -EINVAL;
				goto error_return;
			}
		}
	} else {
		MSS_ERROR_MSG("Not a good interface\n");
		error = -ENXIO;
	}

	MSS_DEBUG_MSG(MEM_SRC, "data buf=%p len=%d inj=%d\n",
		      data_addr,
		      data_length,
		      *injected_data_size);

	MSS_DEBUG_MSG(MEM_SRC, "mdata buf=%p len=%d inj=%d\n",
		      mdata_addr,
		      mdata_length,
		      *injected_mdata_size);

	error = memsrc_update_state(memsrc_obj_p, STM_MEMSRC_STATE_READY);
	if (push_err < 0)
		error = push_err;

error_return:
	return error;
}
EXPORT_SYMBOL(stm_memsrc_push_data_mdata);


/*
 * data and mdata in user address space
 */
static int handle_push_in_user_mode_mdata(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size)
{
	int32_t			retval;
	uint32_t		data_blocks = 0, data_block_count = 0;
	uint32_t		mdata_blocks = 0, mdata_block_count = 0;
	struct scatterlist	*data_sg_list, *mdata_sg_list;
	struct page		**data_input_pages, **mdata_input_pages;
	struct stm_data_block	*data_input_block_list, *mdata_input_block_list;
	uint32_t		i = 0, temp = 0;
	bool bpa_region_data, bpa_region_mdata;

	retval = get_user_block_list(data_block_list,
		      &data_sg_list,
		      &data_input_pages,
		      &data_input_block_list,
		      &data_block_count,
		      &bpa_region_data);
	if (0 > retval)
		return retval;

	retval = get_user_block_list(mdata_block_list,
		      &mdata_sg_list,
		      &mdata_input_pages,
		      &mdata_input_block_list,
		      &mdata_block_count,
		      &bpa_region_mdata);
	if (0 > retval) {
		goto free_data;
	}

	retval = memsrc_obj_p->sink_interface.push_data_mdata(memsrc_obj_p->attach,
		      data_input_block_list,
		      data_block_count,
		      &data_blocks,
		      mdata_input_block_list,
		      mdata_block_count,
		      &mdata_blocks);

	if (bpa_region_data)
		*injected_data_size = (data_blocks == 1) ? data_block_list->len : 0;
	else {
		temp = data_blocks;
		while (temp--) {
			*injected_data_size += data_input_block_list[i].len;
			i++;
		}
	}

	if (bpa_region_mdata)
		*injected_mdata_size = (mdata_blocks == 1) ? mdata_block_list->len : 0;
	else {
		i = 0;
		temp = mdata_blocks;
		while (temp--) {
			*injected_mdata_size += mdata_input_block_list[i].len;
			i++;
		}
	}

	free_user_block_list(mdata_input_pages,
		      mdata_sg_list,
		      mdata_input_block_list,
		      mdata_block_count,
		      mdata_blocks,
		      bpa_region_mdata);

free_data:
	free_user_block_list(data_input_pages,
		      data_sg_list,
		      data_input_block_list,
		      data_block_count,
		      data_blocks,
		      bpa_region_data);

	return retval;

}

/*
 * data in kernel space and mdata in user address space
 */
static int handle_push_in_user_mode_mdata_secure(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size)
{
	int32_t			retval;
	uint32_t		data_blocks = 0;
	uint32_t		mdata_blocks = 0, mdata_block_count = 0;
	struct scatterlist	*mdata_sg_list;
	struct page		**mdata_input_pages;
	struct stm_data_block	*mdata_input_block_list;
	uint32_t		i = 0, temp = 0;
	bool bpa_region_mdata;

	retval = get_user_block_list(mdata_block_list,
		      &mdata_sg_list,
		      &mdata_input_pages,
		      &mdata_input_block_list,
		      &mdata_block_count,
		      &bpa_region_mdata);
	if (0 > retval)
		return retval;

	retval = memsrc_obj_p->sink_interface.push_data_mdata(memsrc_obj_p->attach,
		      data_block_list,
		      1,
		      &data_blocks,
		      mdata_input_block_list,
		      mdata_block_count,
		      &mdata_blocks);

	*injected_data_size = (data_blocks == 1) ? data_block_list->len : 0;

	if (bpa_region_mdata)
		*injected_mdata_size = (mdata_blocks == 1) ? mdata_block_list->len : 0;
	else {
		i = 0;
		temp = mdata_blocks;
		while (temp--) {
			*injected_mdata_size += mdata_input_block_list[i].len;
			i++;
		}
	}


	free_user_block_list(mdata_input_pages,
		      mdata_sg_list,
		      mdata_input_block_list,
		      mdata_block_count,
		      mdata_blocks,
		      bpa_region_mdata);

	return retval;

}

/*
 * data and mdata in vmalloc address space
 */
static int handle_push_in_kernel_vmalloc_mode_mdata(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size)
{
	int32_t			retval;
	uint32_t		data_blocks = 0, data_block_count = 0;
	uint32_t		mdata_blocks = 0, mdata_block_count = 0;
	struct scatterlist	*data_sg_list, *mdata_sg_list;
	struct page		**data_input_pages, **mdata_input_pages;
	struct stm_data_block	*data_input_block_list, *mdata_input_block_list;
	uint32_t		i = 0, temp = 0;

	retval = generate_block_list_vmalloc(data_block_list->data_addr,
		      data_block_list->len,
		      &data_sg_list,
		      &data_input_pages,
		      &data_input_block_list,
		      &data_block_count);
	if (0 > retval)
		return -ENOMEM;

	retval = generate_block_list_vmalloc(mdata_block_list->data_addr,
		      mdata_block_list->len,
		      &mdata_sg_list,
		      &mdata_input_pages,
		      &mdata_input_block_list,
		      &mdata_block_count);
	if (0 > retval) {
		retval = -ENOMEM;
		goto free_data;
	}

	retval = memsrc_obj_p->sink_interface.push_data_mdata(memsrc_obj_p->attach,
		      data_input_block_list,
		      data_block_count,
		      &data_blocks,
		      mdata_input_block_list,
		      mdata_block_count,
		      &mdata_blocks);

	temp = data_blocks;
	while (temp--) {
		*injected_data_size += data_input_block_list[i].len;
		i++;
	}

	i = 0;
	temp = mdata_blocks;
	while (temp--) {
		*injected_mdata_size += mdata_input_block_list[i].len;
		i++;
	}

	free_block_list_vmalloc(mdata_input_pages,
		      mdata_sg_list,
		      mdata_input_block_list,
		      mdata_block_count,
		      mdata_blocks);

free_data:
	free_block_list_vmalloc(data_input_pages,
		      data_sg_list,
		      data_input_block_list,
		      data_block_count,
		      data_blocks);

	return retval;

}

/*
 * data in kernel space and mdata in vmalloc address space
 */
static int handle_push_in_kernel_vmalloc_mode_mdata_secure(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size)
{
	int32_t			retval;
	uint32_t		data_blocks = 0;
	uint32_t		mdata_blocks = 0, mdata_block_count = 0;
	struct scatterlist	*mdata_sg_list;
	struct page		**mdata_input_pages;
	struct stm_data_block	*mdata_input_block_list;
	uint32_t		i = 0, temp = 0;

	retval = generate_block_list_vmalloc(mdata_block_list->data_addr,
		      mdata_block_list->len,
		      &mdata_sg_list,
		      &mdata_input_pages,
		      &mdata_input_block_list,
		      &mdata_block_count);
	if (0 > retval)
		return -ENOMEM;

	retval = memsrc_obj_p->sink_interface.push_data_mdata(memsrc_obj_p->attach,
		      data_block_list,
		      1,
		      &data_blocks,
		      mdata_input_block_list,
		      mdata_block_count,
		      &mdata_blocks);

	*injected_data_size = (data_blocks == 1) ? data_block_list->len : 0;

	i = 0;
	temp = mdata_blocks;
	while (temp--) {
		*injected_mdata_size += mdata_input_block_list[i].len;
		i++;
	}

	free_block_list_vmalloc(mdata_input_pages,
		      mdata_sg_list,
		      mdata_input_block_list,
		      mdata_block_count,
		      mdata_blocks);

	return retval;

}

/*
 * data & mdata in kernel space
 */
static int handle_push_in_kernel_mode_mdata(struct stm_memsrc_s *memsrc_obj_p,
				    struct stm_data_block *data_block_list,
				    uint32_t *injected_data_size,
				    struct stm_data_block *mdata_block_list,
				    uint32_t *injected_mdata_size)
{
	int32_t		ret;
	uint32_t	data_processed = 0, mdata_processed = 0;

	ret = memsrc_obj_p->sink_interface.push_data_mdata(memsrc_obj_p->attach,
		      data_block_list,
		      1,
		      &data_processed,
		      mdata_block_list,
		      1,
		      &mdata_processed);

	*injected_data_size = (data_processed == 1) ? data_block_list->len : 0;
	*injected_mdata_size = (mdata_processed == 1) ? mdata_block_list->len : 0;
	return ret;
}

/* EOF */
