/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/
#include "memutil.h"
#include "mss_debug.h"

int generate_block_list(char *buf, int32_t count,
			struct scatterlist **input_sg_list,
			struct page ***input_pages,
			struct stm_data_block **input_block_list,
			uint32_t *pages_mapped)
{
	struct page **pages;
	struct scatterlist *sg_list;
	struct stm_data_block *block_list;
	unsigned long uaddr = (unsigned long) buf;
	int32_t npages;
	uint32_t initial_offset;
	uint32_t i;
	int ret;

	initial_offset = uaddr & ~PAGE_MASK;
	npages = (initial_offset + count + PAGE_SIZE - 1) >> PAGE_SHIFT;

	pages = infra_os_malloc(npages * sizeof(*pages));
	if (!pages) {
		pr_err("Pages not allocated\n");
		goto error_pages;
	}

	ret = get_user_pages_fast(uaddr, npages, 1, pages);
	if (ret < 0)
		goto error_mapped;

	npages = ret;

	if (WARN_ON(npages == 0))
		goto error_mapped;

	sg_list = infra_os_malloc(npages * sizeof(*sg_list));
	if (!sg_list) {
		pr_err("SG list not allocated\n");
		goto error_list;
	}
	sg_init_table(sg_list, npages);

	block_list = infra_os_malloc(npages * sizeof(*block_list));
	if (!block_list) {
		pr_err("block_list not allocated\n");
		goto error_block;
	}

	for (i = 0; i < npages; i++) {
		int plen = min_t(int, count, PAGE_SIZE - initial_offset);

		sg_set_page(sg_list + i, pages[i], plen, initial_offset);

#ifdef CONFIG_HIGHMEM
		block_list[i].data_addr = kmap(sg_page(sg_list + i))+(sg_list+i)->offset;
#else
		block_list[i].data_addr = sg_virt(sg_list + i);
#endif
		block_list[i].len = plen;
		block_list[i].next = &(block_list[i+1]);

		initial_offset = 0;
		count -= plen;
		ret += plen;
	}
	block_list[i-1].next = NULL;
	*input_pages = pages;
	*input_sg_list = sg_list;
	*input_block_list = block_list;
	*pages_mapped = npages;

	return ret;

error_block:
	infra_os_free(sg_list);

error_list:
	while (--npages >= 0)
		page_cache_release(pages[npages]);

error_mapped:
	infra_os_free(pages);
error_pages:
	return -ENOMEM;
}

void free_block_list(struct page **pages,
		     struct scatterlist *sg_list,
		     struct stm_data_block *input_block_list,
		     uint32_t mapped,
		     uint32_t filled_blocks)
{
	int index;

	for (index = 0; index < mapped; index++) {
#ifdef CONFIG_HIGHMEM
		kunmap(sg_page(sg_list+index));
#endif
		flush_kernel_dcache_page(pages[index]);
		page_cache_release(pages[index]);
	}

	infra_os_free(input_block_list);
	input_block_list = NULL;
	infra_os_free(sg_list);
	sg_list = NULL;
	infra_os_free(pages);
	pages = NULL;
}

int generate_block_list_vmalloc(char *buf, int32_t count,
				struct scatterlist **input_sg_list,
				struct page ***input_pages,
				struct stm_data_block **input_block_list,
				uint32_t *pages_mapped)
{
	struct scatterlist *sglist;
	struct page **pages = NULL;
	struct stm_data_block *block_list;
	int32_t npages;
	uint32_t initial_offset;
	uint32_t i;
	int ret = 0;
	int plen = 0;

	initial_offset = (unsigned long) buf & ~PAGE_MASK;
	npages = (initial_offset + count + PAGE_SIZE - 1) >> PAGE_SHIFT;

	pages = infra_os_malloc(npages * sizeof(*pages));
	if (!pages) {
		pr_err("Pages not allocated\n");
		goto error_pages;
	}
	sglist = infra_os_malloc(npages * sizeof(*sglist));
	if (sglist == NULL) {
		pr_err("%s: fail in vzalloc sglist\n", __func__);
		goto error_sglist;
	}
	block_list = infra_os_malloc(npages * sizeof(*block_list));
	if (!block_list) {
		pr_err("%s: blocklist not allocated\n", __func__);
		goto error_block;
	}
	sg_init_table(sglist, npages);
	for (i = 0; i < npages; i++, buf += plen) {
		plen = min_t(int, count, PAGE_SIZE - initial_offset);
		pages[i] = vmalloc_to_page(buf);
		if (NULL == pages[i]) {
			pr_err("%s fail vmalloc_to_page\n", __func__);
			goto error_list;
		}
		if(PageHighMem(pages[i])) {
			pr_err("%s Page in high mem\n", __func__);
			goto error_list;
		}
		sg_set_page(&sglist[i], pages[i], plen, initial_offset);

		block_list[i].data_addr = sg_virt(&sglist[i]);
		block_list[i].len = plen;
		block_list[i].next = &(block_list[i+1]);

		initial_offset = 0;
		count -= plen;
		ret += plen;
	}
	block_list[i-1].next = NULL;
	*input_pages = pages;
	*input_sg_list = sglist;
	*input_block_list = block_list;
	*pages_mapped = npages;

	return ret;

error_list:
	infra_os_free(block_list);
error_block:
	infra_os_free(sglist);

error_sglist:
	infra_os_free(pages);
error_pages:
	return -ENOMEM;
}

void free_block_list_vmalloc(struct page **pages,
			     struct scatterlist *sg_list,
			     struct stm_data_block *input_block_list,
			     uint32_t mapped,
			     uint32_t filled_blocks)
{
	int index;

	for (index = 0; index < mapped; index++)
		flush_kernel_dcache_page(pages[index]);

	infra_os_free(input_block_list);
	input_block_list = NULL;
	infra_os_free(sg_list);
	sg_list = NULL;
	infra_os_free(pages);
	pages = NULL;
}

static unsigned long get_physical_contiguous(unsigned long ptr, size_t size)
{
	struct mm_struct *mm = current->mm;
	unsigned long virt_base = (ptr / PAGE_SIZE) * PAGE_SIZE;
	unsigned long phys_base = 0;

	pgd_t *pgd;
	pmd_t *pmd;
	pte_t *ptep, pte;

	MSS_DEBUG_MSG(MEM_UTILS, "User pointer = %p\n", (void *)ptr);

	if (!ptr || !size)
		return 0;

	spin_lock(&mm->page_table_lock);

	pgd = pgd_offset(mm, virt_base);
	if (pgd_none(*pgd) || pgd_bad(*pgd))
		goto out;

	pmd = pmd_offset((pud_t *) pgd, virt_base);
	if (pmd_none(*pmd) || pmd_bad(*pmd))
		goto out;

	ptep = pte_offset_map(pmd, virt_base);

	if(!ptep)
		goto out;

	pte = *ptep;

	if (pte_present(pte)){
		 phys_base = page_to_phys(pfn_to_page(pte_pfn(pte))) | (virt_base & (PAGE_SIZE-1));
	}

	if (!phys_base)
		goto unmap_out;

	pte_unmap(ptep);
	spin_unlock(&mm->page_table_lock);
	return phys_base + (ptr - virt_base);

unmap_out:
	pte_unmap(ptep);
out:
	spin_unlock(&mm->page_table_lock);
	return 0;
}

static void *get_bpa2_part_addr(struct stm_data_block *block_list)
{
	struct bpa2_part *part;
	void *addr = NULL;
	unsigned long base;

	base = get_physical_contiguous((unsigned int)block_list->data_addr, block_list->len);
	if (!base)
		goto out;

	part = bpa2_find_part_addr(base, block_list->len);

	if (part) {
		if (bpa2_low_part(part))
			addr = phys_to_virt(base);
		else
			addr = ioremap_nocache(base, block_list->len);
	}

out:
	return addr;
}

int get_user_block_list(struct stm_data_block *input_block_list,
					struct scatterlist **input_sg_list,
					struct page ***input_pages,
					struct stm_data_block **generated_block_list,
					int *mapped, bool *region_is_bpa2)
{
	int retval = 0;
	void *bpa_addr;
	struct stm_data_block *block_list;

	bpa_addr =  get_bpa2_part_addr(input_block_list);

	if (bpa_addr) {
		block_list = infra_os_malloc(sizeof(*block_list));
		if (!block_list) {
			MSS_ERROR_MSG("block_list not allocated\n");
			retval = -ENOMEM;
			goto error;
		}

		block_list->data_addr = bpa_addr;
		block_list->len = input_block_list->len;

		*generated_block_list = block_list;
		*mapped = 1;
		*region_is_bpa2 = true;
	} else {
		*region_is_bpa2 = false;
		retval = generate_block_list(input_block_list->data_addr,
				  input_block_list->len,
				  input_sg_list,
				  input_pages,
				  generated_block_list,
				  mapped);
		if (0 > retval) {
			retval = -ENOMEM;
			goto error;
		}
	}

error:
	return retval;
}

void free_user_block_list(struct page **pages,
		     struct scatterlist *sg_list,
		     struct stm_data_block *input_block_list,
		     uint32_t mapped,
		     uint32_t filled_blocks,
		     bool region_is_bpa2)
{
	if (region_is_bpa2) {
		infra_os_free(input_block_list);
		input_block_list = NULL;
	}
	else
		free_block_list(pages,
			  sg_list,
			  input_block_list,
			  mapped,
			  filled_blocks);
}

static int __init stm_memsrcsink_init(void)
{
	stm_memsrc_init();
	stm_memsink_init();
	return 0;
}
module_init(stm_memsrcsink_init);

static void __exit stm_memsrcsink_term(void)
{
	stm_memsrc_term();
	stm_memsink_term();
}
module_exit(stm_memsrcsink_term);
